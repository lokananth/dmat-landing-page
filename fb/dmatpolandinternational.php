﻿<?php if(!isset($_SESSION)){session_start();} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" >
<title>Pakiet międzynarodowy   </title> <meta name="description" content="Pakiet międzynarodowy do Polski" /><meta name="keywords" content="Pakiet międzynarodowy do Polski na numery komórkowe ,500 minut do Polski na numery komórkowe, tylko €10, Darmowa kart SIM Delight, Zamów darmową kartę SIM, Delight mobile,  Darmowy internet, Transfer doładowania, Newsy sportowe, Delight działa na infrastrukturze KPN" /><meta name="robots" content="no index, no follow" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- ******************SEO Metrics Start***************** -->

    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
     <link type="text/css" rel="stylesheet" href="../ppc/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900' rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet" href="../ppc/css/bootstrap.css">
<link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico" />

<script type="text/javascript">
    function toggle() {
        var ele = document.getElementById("toggleText");
        var text = document.getElementById("displayText");
        if (ele.style.display == "block") {
            ele.style.display = "none";
            text.className = "";
            text.className = "teamsconditions_pluss";
            text.innerHTML = "Allgemeine Geschäftsbedingungen [+]"
        }
        else {
            ele.style.display = "block";
            text.className = "";
            text.className = "teamsconditions_plusss";
            text.innerHTML = "Allgemeine Geschäftsbedingungen [-]"
        }
    } 
</script>
<script type="text/javascript">
    WebFontConfig = {
        google: { families: ['Roboto:400,500,700,900:latin'] }
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })(); </script>
<!-- Google Analytics Code Tag End -->
<!-- ******************SEO Metrics End****************** -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MVLXL3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({ 'gtm.start':
new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MVLXL3');</script>
<!-- End Google Tag Manager -->
   <form id="form1" runat="server">
    <div id="wrapper">
        <div class="landing_pages">
            <div class="header_top_in">
                <div class="top-left-img">
                </div>
                <div class="title container-wi">
                    <div class="logo">
                        <strong class="logoimg">Vectone Mobile</strong>
                         <h3 class="h3content">
                      	Pakiet międzynarodowy</h3>
                    </div>
                    
                </div>
        
                <div class="container-wi">
                    <div class="frame Singapore">
                        <div class="biggest polandi">
                            <h1>
                                500 <span><span>minut do <strong>Polski</strong></span> </span>
                            </h1>
                            <img class="globeflag-in" src="/ppc/img-lp/Poland-FLag-in.png" />
                           
                        </div>
                    </div>
                    <img class="sim99in" src="/ppc/img-lp/DMAT_SIM_Poland.png" />
                </div>
            </div>
            <div class="arrow-down">
            </div>
            <div class="section">
                <div class="container-wi">
                    <div class="row-fluid">
                        <!-- form start -->

        <?php include '../ppc/controls/international-po.php'; ?>

<!-- form end-->
                        <div class="span6">
                            <div class="Lp-div-sub">
                                <div class="free_sim_right_sub">
                                    <img alt="" src="/ppc/img-lp/features1.png" class="lp-img-1"></div>
                                <div class="free_sim_right_sub2">
                                    <strong>Ciesz się Internetem mobilnym za darmo!  </strong>
                                    <p>
                                      Korzystaj z darmowego Interenetu przy każdym doładowaniu. Bądź na bieżąco z aktualnościami, sprawdź maila lub Facebooka i surfuj po Internecie. Czas na więcej za mniejsze pieniądze! </p>
                                </div>
                            </div>
                            <div class="Lp-div-sub">
                                <div class="free_sim_right_sub">
                                    <img alt="" src="/ppc/img-lp/features2.png" class="lp-img-1"></div>
                                <div class="free_sim_right_sub2">
                                    <strong>Doładuj konto swoich bliskich   </strong>
                                    <p>
                                    Z Delight Mobile możesz szybko, bezpieczne i komfortowo doładować konto Twoich najbliższych. </p>
                                </div>
                            </div>
                            <div class="Lp-div-sub">
                                <div class="free_sim_right_sub">
                                    <img alt="" src="/ppc/img-lp/features3.png" class="lp-img-1"></div>
                                <div class="free_sim_right_sub2">
                                    <strong>Najnowsze wiadomości sportowe  </strong>
                                    <p>
                                     Zasięgnij najciekawszych informacji sportowych już od 25 centów za minutę. Nic prostszego: zadzwoń na 06889 000 061 z Twojego numeru Delight Mobile i wybierz odpowiednią opcję. </p>
                                </div>
                            </div>
                          
                      
                        </div>
                    </div>
                </div>
            </div>
            <div class="subcontent">
                <div class="container-wi">
                  <div class="SecondFooter">
            <p>
                <a class="teamsconditions_pluss" href="javascript:toggle();" id="displayText">Regulamin [+]</a></p>
            <div style="display: none;" id="toggleText">
                <ul>
                     <li>Pakiet międzynarodowy Delight pozwala na wykonywanie połączeń z Austrii na numery komórkowe oraz stacjonarne do następujących krajów: Rumunia, Polska, Węgry, Bułgaria, Litwa, Łotwa, Indie, Chiny, Tajlandia, Belgia, Czechy, Niemcy.</li>
  <li>W przypadku wykorzystania wszystkich jednostek z pakietu, obowiązują standarowe stawki operatora. </li>
  <li>Minuty i SMS-y zawarte w pakiecie nie mogą być wykorzystywane w roamingu oraz do połączeń z numerami specjalnymi i premium.</li>
  <li>Pakiety zachowują ważność przez 30 dni od momentu ich  aktywacji. Po upływie 30 dni niewykorzystane minuty, wiadomości i pakiety  danych tracą ważność. </li>
  <li>Obowiązuje naliczanie minutowe.</li>
  <li>Jednocześnie można korzystać tylko z jednego pakietu.  Wykupienie nowego pakietu możliwe jest dopiero w momencie zakończenia ważności  poprzedniego pakietu (tj. po 30 dniach).</li>
  <li>Delight zastrzega sobie prawo do wycofania lub zmiany  warunków oferty w dowolnej chwili.</li>
  <li>Kredyt promocyjny (otrzymany od Vectone za darmo) nie  może zostać wykorzystany do kupienia pakietu.</li>
  <li>Klienci, którzy subskrybują Pakiet Międzynarodowy nie  mogą skorzystać z promocji Smart Rates, do momentu wygaśnięcia ważności  pakietu.</li>
  <li>Oferta skierowana jest wyłącznie użytku prywatnego,  niekomercyjnego. Delight zastrzega sobie prawo do odmówienia lub zawieszenia  świadczenia usług w przypadku podejrzenia, że usługa wykorzystywana jest w  celach komercyjnych, konferencyjnych lub jeśli nie jest używana w telefonie  komórkowym, a np. jako tethering lub w jakimkolwiek innym celu, który Delight  uzna za sprzeczny z niniejszym regulaminem.</li>
  <li>Żeby zamówić usługę, wybierz *3113#</li>
                   
                   
                </ul>
            
           
        </div>
        </div>
                </div>
            </div>
         
            <div class="section section-bottom pad-b">
                <div class="footer-bg">
                    <div class="container-wi">
                        <div class="row-fluid">
                            <div class="span3">
                                <h3>
                                    <strong>Trzy proste kroki, aby do nas dołączyć: </strong></h3>
                                </div>
                            <div class="span3">
                                <div class="alert alert-text alert-label alert-label-1">
                                
                                 Wypełnij formularz a w ciągu 3-5 dni roboczych otrzymasz darmową kartę SIM 2 w 1  </div>
                            </div>
                            <div class="span3">
                                <div class="alert alert-text alert-label alert-label-2">
                                
                                Kiedy już otrzymasz kartę SIM, umieść ją w telefonie bez blokady </div>
                            </div>
                            <div class="span3">
                                <div class=" alert alert-text alert-label alert-label-3">
                                  "Witamy". Jesteś już połączony </div>
                            </div>
                        </div>
                        <p>
                            Zasady i Warunki obowiązują. <br />Po więcej informacji odwiedź 
<a href="http://www.delightmobile.at" target="_blank">www.delightmobile.at</a>
</p>
                    </div>
               
                </div>

            </div>
                     <footer>
                    <div class="container-wi">
                   
                        
                                <div class="powered">Delight benutzt das Netz von T-mobile	</div>
                                
                                <div class="pull-right">
                             
                               <div class="copyright">
                         
                                Copyright &copy; 2015 Mundio</div>
                              
                            </div>
                        
                    </div>
                </footer>
                </div>
                </div>
    </form>
    </body>
</html>


