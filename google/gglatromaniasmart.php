﻿<?php if(!isset($_SESSION)){session_start();} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" >
<title>Tarife Smart Romania</title> <meta name="description" content=" Tarife Smart Romania" /><meta name="keywords" content=" Suna in Romania pe Mobil 1ct/min , Delight sim gratis, trimite cartela sim gratis, Delight mobile,  Tarife smart, Trei pasi simpli pentru a ni te alatura, Stiri din sport, Internet pe Mobil Gratis, Transfer de credit, Delight functioneaza in reteaua T-Mobile, Comanda cartela Standard / Micro / Nano SIM gratis, Trimite-mi cartela SIM gratis, apeluri si sms-uri gratis in reteaua Delight" /><meta name="robots" content="no index, no follow" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- ******************SEO Metrics Start***************** -->

    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
     <link type="text/css" rel="stylesheet" href="../ppc/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900' rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet" href="../ppc/css/bootstrap.css">
<link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico" />

<script type="text/javascript">
    function toggle() {
        var ele = document.getElementById("toggleText");
        var text = document.getElementById("displayText");
        if (ele.style.display == "block") {
            ele.style.display = "none";
            text.className = "";
            text.className = "teamsconditions_pluss";
            text.innerHTML = "Allgemeine Geschäftsbedingungen [+]"
        }
        else {
            ele.style.display = "block";
            text.className = "";
            text.className = "teamsconditions_plusss";
            text.innerHTML = "Allgemeine Geschäftsbedingungen [-]"
        }
    } 
</script>
<script type="text/javascript">
    WebFontConfig = {
        google: { families: ['Roboto:400,500,700,900:latin'] }
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })(); </script>
<!-- Google Analytics Code Tag End -->
<!-- ******************SEO Metrics End****************** -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MVLXL3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({ 'gtm.start':
new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MVLXL3');</script>
<!-- End Google Tag Manager -->

<form id="form1" runat="server">
    <div id="wrapper">
        <div class="landing_pages">
            <div class="header_top">
                <div class="top-left-img">
                </div>
                <div class="title container-wi">
                    <div class="logo">
                        <strong class="logoimg">Vectone Mobile</strong>
                         <h3 class="h3content">
                       Tarife Smart </h3>
                    </div>
                    
                </div>
        
                <div class="container-wi">
                    <div class="frame Romania">
                        <div class="biggest">
                            <h1>
                                1 <span>ct/<span>min</span></span>
                            </h1>
                            <img class="globeflag" src="/ppc/img-lp/Romania-FLag.png" />
                            <h2>
                                
                                <br class="visible-mobile" />
                               Sună în <strong> România </strong>pe Mobil</h2>
                        </div>
                    </div>
                    <div class="ratesdrop spec">
                        <div class="row-fluid">
                         <h3> <span>+ </span>Suni pe gratis în reţeaua Delight  <br /><i>(când reîncarci)</i></h3>
                      
                        </div>
                     
                    </div>
                </div>
            </div>
            <div class="arrow-down">
            </div>
            <div class="section">
                <div class="container-wi">
                    <div class="row-fluid">
                        <!-- form start -->

        <?php include '../ppc/controls/international-ro.php'; ?>

<!-- form end-->
                        <div class="span6">
                            <div class="Lp-div-sub">
                                <div class="free_sim_right_sub">
                                    <img alt="" src="/ppc/img-lp/features1.png" class="lp-img-1"></div>
                                <div class="free_sim_right_sub2">
                                    <strong>Te bucuri de Internet pe Mobil GRATIS în fiecare zi </strong>
                                    <p>
                                       Primeşti mereu mai mult Internet când reîncarci. Navighezi pe Facebook, verifici e-mailul, urmăreşti ştirile, sau pur şi simplu navighezi pe Internet. E timpul să primeşti mai mult pentru banii tăi.</p>
                                </div>
                            </div>
                            <div class="Lp-div-sub">
                                <div class="free_sim_right_sub">
                                    <img alt="" src="/ppc/img-lp/features2.png" class="lp-img-1"></div>
                                <div class="free_sim_right_sub2">
                                    <strong>Transferă credit celor dragi   </strong>
                                    <p>
                                     Transferă credit cu Delight Mobile oriunde în lume, direct pe telefoanele mobile a celor dragi. Sistemul nostru de transfer de credit este rapid, confortabil şi uşor de utilizat.</p>
                                </div>
                            </div>
                            <div class="Lp-div-sub">
                                <div class="free_sim_right_sub">
                                    <img alt="" src="/ppc/img-lp/features3.png" class="lp-img-1"></div>
                                <div class="free_sim_right_sub2">
                                    <strong>Utimele ştiri din sport </strong>
                                    <p>
                                     Primeşti pe mobilul tău Delight cele mai recente ştiri despre sporturile şi echipele tale favorite, începând de la 25 cenţi pe minut. Este simplu: Doar apelează 06889 000 061 de pe numărul tău de mobil Delight şi optează pentru ştiri sportive. Astfel poţi urmări evenimentele tale favorite din mers.</p>
                                </div>
                            </div>
                          
                      
                        </div>
                    </div>
                </div>
            </div>
            <div class="subcontent">
                <div class="container-wi">
                       <div class="SecondFooter">
            <p>
                <a class="teamsconditions_pluss" href="javascript:toggle();" id="displayText">Termeni şi condiţii  [+]</a></p>
            <div style="display: none;" id="toggleText">
                <ul>
                    <li>Tarifele Smart sunt valabile doar pentru clienţii care s-au alăturat reţelei noastre pe sau după data de 01.01.2015</li>
  <li>Pentru a beneficia de tarifele Smart, trebuie să trimiteţi un SMS cu textul 345 în fiecare lună calendaristică cu reîncărcare minimă</li>
                   
                </ul>
            
           
        </div>
        </div>
                </div>
            </div>
         
            <div class="section section-bottom pad-b">
                <div class="footer-bg">
                    <div class="container-wi">
                        <div class="row-fluid">
                            <div class="span3">
                                <h3>
                                    <strong>Trei paşi simpli pentru a ni te alătura </strong></h3>
                                </div>
                            <div class="span3">
                                <div class="alert alert-text alert-label alert-label-1">
                               
                                Completează formularul pentru a primi cartela ta SIM 3 în 1 GRATIS în 3-5 zile lucrătoare</div>
                            </div>
                            <div class="span3">
                                <div class="alert alert-text alert-label alert-label-2">
                                 
                                  Când primeşti cartela SIM, introdu-o într-un telefon deblocat</div>
                            </div>
                            <div class="span3">
                                <div class=" alert alert-text alert-label alert-label-3"> 
                                  Eşti conectat cu Delight </div>
                            </div>
                        </div>
                        <p>
                        Se aplică termeni şi condiţii. Pentru mai multe <br />informaţii vă rugăm accesaţi 
<a href="http://www.delightmobile.at" target="_blank">www.delightmobile.at</a>
</p>
                    </div>
               
                </div>

            </div>
                     <footer>
                    <div class="container-wi">
                   
                        
                                <div class="powered">Delight funcţionează în reţeaua T-mobile	</div>
                                
                                <div class="pull-right">
                             
                               <div class="copyright">
                               
                               
                               
                               
                               

                             
                         
                                Copyright &copy; 2015 Mundio</div>
                              
                            </div>
                        
                    </div>
                </footer>
                </div>
                </div>
    </form>
    </body>
</html>


