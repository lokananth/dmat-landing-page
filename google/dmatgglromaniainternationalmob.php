﻿<?php if(!isset($_SESSION)){session_start();} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" >
<title>Pachetul Internaţional </title> <meta name="description" content="Pachetul Internaţional" /><meta name="keywords" content="Pachetul Internaţional România Mobil , 500 minute către România pe Mobil, doar €10, sim gratis delight, trimite-mi sim gratis, Delight mobile, internet Gratis, transfer de credit, ştiri din sport, Delight funcţionează pe reţeaua KPN" /><meta name="robots" content="no index, no follow" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- ******************SEO Metrics Start***************** -->

    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
     <link type="text/css" rel="stylesheet" href="../ppc/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900' rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet" href="../ppc/css/bootstrap.css">
<link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico" />

<script type="text/javascript">
    function toggle() {
        var ele = document.getElementById("toggleText");
        var text = document.getElementById("displayText");
        if (ele.style.display == "block") {
            ele.style.display = "none";
            text.className = "";
            text.className = "teamsconditions_pluss";
            text.innerHTML = "Allgemeine Geschäftsbedingungen [+]"
        }
        else {
            ele.style.display = "block";
            text.className = "";
            text.className = "teamsconditions_plusss";
            text.innerHTML = "Allgemeine Geschäftsbedingungen [-]"
        }
    } 
</script>
<script type="text/javascript">
    WebFontConfig = {
        google: { families: ['Roboto:400,500,700,900:latin'] }
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })(); </script>
<!-- Google Analytics Code Tag End -->
<!-- ******************SEO Metrics End****************** -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MVLXL3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({ 'gtm.start':
new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MVLXL3');</script>
<!-- End Google Tag Manager -->

<form id="form1" runat="server">
    <div id="wrapper">
        <div class="landing_pages">
            <div class="header_top_in">
                <div class="top-left-img">
                </div>
                <div class="title container-wi">
                    <div class="logo">
                        <strong class="logoimg">Vectone Mobile</strong>
                         <h3 class="h3content">
                       Pachetul Internaţional  </h3>
                    </div>
                    
                </div>
        
                <div class="container-wi">
                         <div class="frame Romania-in">
                        <div class="biggest">
                            <h1>
                                500 <span><span>minute</span></span>
                            </h1>
                            <img class="globeflag-in" src="/ppc/img-lp/Romania-FLag-in.png" />
                            <h2>
                                
                                <br class="visible-mobile" />către <strong class="inline"> România pe mobil</strong></h2>
                        </div>
                    </div>
                    <img class="sim99in" src="/ppc/img-lp/DMAT_SIM_Romania.png" />
                    
                </div>
            </div>
            <div class="arrow-down">
            </div>
            <div class="section">
                <div class="container-wi">
                    <div class="row-fluid">
                         <!-- form start -->

        <?php include '../ppc/controls/international-ro.php'; ?>

<!-- form end-->
                        <div class="span6">
                            <div class="Lp-div-sub">
                                <div class="free_sim_right_sub">
                                    <img alt="" src="/ppc/img-lp/features1.png" class="lp-img-1"></div>
                                <div class="free_sim_right_sub2">
                                    <strong>Te bucuri de Internet pe Mobil GRATIS în fiecare zi </strong>
                                    <p>
                                       Primeşti mereu mai mult Internet când reîncarci. Navighezi pe Facebook, verifici e-mailul, urmăreşti ştirile, sau pur şi simplu navighezi pe Internet. E timpul să primeşti mai mult pentru banii tăi.</p>
                                </div>
                            </div>
                            <div class="Lp-div-sub">
                                <div class="free_sim_right_sub">
                                    <img alt="" src="/ppc/img-lp/features2.png" class="lp-img-1"></div>
                                <div class="free_sim_right_sub2">
                                    <strong>Transferă credit celor dragi   </strong>
                                    <p>
                                     Transferă credit cu Delight Mobile oriunde în lume, direct pe telefoanele mobile a celor dragi. Sistemul nostru de transfer de credit este rapid, confortabil şi uşor de utilizat.</p>
                                </div>
                            </div>
                            <div class="Lp-div-sub">
                                <div class="free_sim_right_sub">
                                    <img alt="" src="/ppc/img-lp/features3.png" class="lp-img-1"></div>
                                <div class="free_sim_right_sub2">
                                    <strong>Utimele ştiri din sport </strong>
                                    <p>
                                     Primeşti pe mobilul tău Delight cele mai recente ştiri despre sporturile şi echipele tale favorite, începând de la 25 cenţi pe minut. Este simplu: Doar apelează 06889 000 061 de pe numărul tău de mobil Delight şi optează pentru ştiri sportive. Astfel poţi urmări evenimentele tale favorite din mers.</p>
                                </div>
                            </div>
                          
                      
                        </div>
                    </div>
                </div>
            </div>
            <div class="subcontent">
                <div class="container-wi">
                       <div class="SecondFooter">
            <p>
                <a class="teamsconditions_pluss" href="javascript:toggle();" id="displayText">Termeni şi condiţii  [+]</a></p>
            <div style="display: none;" id="toggleText">
                <ul>
                    <li>Beneficiile incluse în Pachetele Internaţionale Delight se referă la apeluri din Austria către numere de fix şi mobil din Bulgaria, China, Republica Cehă, Belgia, Germania, Polonia, România şi Thailanda. Alte moduri de utilizare, inclusiv apelurile către numere premium şi utilizarea dincolo de limita din ofertă vor fi taxate la tariful standard. Această ofertă nu poate fi folosită în roaming.</li>
  <li>Beneficiile sunt valabile 30 de zile de la  data achiziţionării. Apelurile sunt rotunjite la cel mai apropiat minut de  calcul. Un client poate avea numai o singură ofertă pe contul său şi nu poate  achiziţiona alta în perioada de valabilitate de 30 de zile. Minutele rămase  după perioada de 30 de zile nu pot fi transferate în următoarea perioadă de 30  de zile chiar dacă este reînnoit pachetul.</li>
  <li>Delight îşi rezervă dreptul de a modifica  sau încheia această ofertă sau aceşti Termeni şi Condiţii în orice moment cu  notificare rezonabilă.</li>
  <li>Vă rugăm să reţineţi faptul că în cazul  creditului promoţional (Credit oferit de către Delight pe gratis clienţilor),  acesta nu poate fi utilizat pentru a achiziţiona un pachet. De asemenea,  clienţii care au achiziţionat Pachetul Internaţional nu pot beneficia de Smart  Rates până la expirarea perioadei de valabilitate.</li>
  <li>Oferta este pentru uz non-comercial,  privat, strict personal – Delight Mobile își rezervă dreptul de a retrage sau  suspenda serviciile sau să vă deconecteze dacă există suspiciuni că oferta este  folosită în scopuri comerciale, pentru conferințe, sau în cazul în care nu este  utilizată într-un receptor (într-o cutie SIM de exemplu), sau în cazul în care  este folosit pentru a partaja Internet de pe telefon pe alte dispozitive  electronice. De asemenea, acest lucru se poate intampla din orice alt motiv  rezonabil pentru care Delight consideră că nu acţionaţi în conformitate cu această  politică, considerând că utilizarea a fost nelegitimă. </li>
  <li>Pentru a cumpăra, vă rugăm apelaţi *3113# </li>
                   
                </ul>
            
           
        </div>
        </div>
                </div>
            </div>
         
            <div class="section section-bottom pad-b">
                <div class="footer-bg">
                    <div class="container-wi">
                        <div class="row-fluid">
                            <div class="span3">
                                <h3>
                                    <strong>Trei paşi simpli pentru a ni te alătura </strong></h3>
                                </div>
                            <div class="span3">
                                <div class="alert alert-text alert-label alert-label-1">
                               
                                Completează formularul pentru a primi cartela ta SIM 2 în 1 GRATIS în 3-5 zile lucrătoare</div>
                            </div>
                            <div class="span3">
                                <div class="alert alert-text alert-label alert-label-2">
                                 
                                  Când primeşti cartela SIM, introdu-o într-un telefon deblocat</div>
                            </div>
                            <div class="span3">
                                <div class=" alert alert-text alert-label alert-label-3"> 
                                  Eşti conectat cu Delight </div>
                            </div>
                        </div>
                        <p>
                        Se aplică termeni şi condiţii. Pentru mai multe <br />informaţii vă rugăm accesaţi 
<a href="http://www.delightmobile.at" target="_blank">www.delightmobile.at</a>
</p>
                    </div>
               
                </div>

            </div>
                     <footer>
                    <div class="container-wi">
                   
                        
                                <div class="powered">Delight funcţionează în reţeaua T-mobile	</div>
                                
                                <div class="pull-right">
                             
                               <div class="copyright">
                               
                               
                               
                               
                               
                             
                         
                                Copyright &copy; 2015 Mundio</div>
                              
                            </div>
                        
                    </div>
                </footer>
                </div>
                </div>
    </form>
    </body>
</html>


