﻿<?php if(!isset($_SESSION)){session_start();} ?>
<!--     **********ppc validation start*************** -->

    <script src="../ppc/js/validation.js" type="text/javascript"></script>

<!--  **********ppc validation end***************  -->
<div class="span6 grayb">
                          
 
            
 <script src="../Scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#various').click(function () {
            $('#inline').fadeIn();
        });
        $('#fancyBoxClose').click(function () {
            $('#inline').hide();
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //FirstName
        $('#ucFreeSimLandingPages_TextBoxFirstName').blur(function () {
            var Fristname = $('#ucFreeSimLandingPages_TextBoxFirstName').val();
            if (Fristname.length > 0) {
                $('#ucFreeSimLandingPages_divFirstName').addClass("empty");
                $('#ucFreeSimLandingPages_divFirstName').removeClass("block errormsg");
                $('#ucFreeSimLandingPages_divFirstName').text('');
            }
            else {
                $('#ucFreeSimLandingPages_divFirstName').removeClass("empty");
                $('#ucFreeSimLandingPages_divFirstName').addClass("block errormsg");
                $('#ucFreeSimLandingPages_divFirstName').html('Wpisz swoje imię.');
            }
        });

        //LastName
        $('#ucFreeSimLandingPages_TextBoxLastName').blur(function () {
            var lastname = $('#ucFreeSimLandingPages_TextBoxLastName').val();
            if (lastname.length > 0) {
                $('#ucFreeSimLandingPages_divLastName').addClass("empty");
                $('#ucFreeSimLandingPages_divLastName').removeClass("block errormsg");
                $('#ucFreeSimLandingPages_divLastName').text('');
            }
            else {
                $('#ucFreeSimLandingPages_divLastName').removeClass("empty");
                $('#ucFreeSimLandingPages_divLastName').addClass("block errormsg");
                $('#ucFreeSimLandingPages_divLastName').html('Wpisz swoje nazwisko.');
            }
        });

        //House number
        $('#ucFreeSimLandingPages_TextBoxHouseNumber').blur(function () {
            var Housenumber = $('#ucFreeSimLandingPages_TextBoxHouseNumber').val();
            if (Housenumber.length > 0) {
                $('#ucFreeSimLandingPages_divHouseNumber').addClass("empty");
                $('#ucFreeSimLandingPages_divHouseNumber').removeClass("block errormsg clear");
                $('#ucFreeSimLandingPages_divHouseNumber').text('');
            }
            else {
                $('#ucFreeSimLandingPages_divHouseNumber').removeClass("empty");
                $('#ucFreeSimLandingPages_divHouseNumber').addClass("block errormsg clear");
                $('#ucFreeSimLandingPages_divHouseNumber').html('Wpisz numer domu.');
            }
        });

        //Street name
        $('#ucFreeSimLandingPages_TextBoxStreet').blur(function () {
            var Street = $('#ucFreeSimLandingPages_TextBoxStreet').val();
            if (Street.length > 0) {
                $('#ucFreeSimLandingPages_divStreet').addClass("empty");
                $('#ucFreeSimLandingPages_divStreet').removeClass("block errormsg clear");
                $('#ucFreeSimLandingPages_divStreet').text('');
            }
            else {
                $('#ucFreeSimLandingPages_divStreet').removeClass("empty");
                $('#ucFreeSimLandingPages_divStreet').addClass("block errormsg clear");
                $('#ucFreeSimLandingPages_divStreet').html('Wpisz nazwę ulicy.');
            }
        });

        //Town name
        $('#ucFreeSimLandingPages_TextBoxCity').blur(function () {
            var City = $('#ucFreeSimLandingPages_TextBoxCity').val();
            if (City.length > 0) {
                $('#ucFreeSimLandingPages_divTown').addClass("empty");
                $('#ucFreeSimLandingPages_divTown').removeClass("block errormsg clear");
                $('#ucFreeSimLandingPages_divTown').text('');
            }
            else {
                $('#ucFreeSimLandingPages_divTown').removeClass("empty");
                $('#ucFreeSimLandingPages_divTown').addClass("block errormsg clear");
                $('#ucFreeSimLandingPages_divTown').html('Wpisz nazwę miasta.');
            }
        });

        //PostalCode
        $('#ucFreeSimLandingPages_TextBoxPostCode').blur(function () {
            var PostCode = $('#ucFreeSimLandingPages_TextBoxPostCode').val();
            if (PostCode.length > 0) {
                $('#ucFreeSimLandingPages_divPostCode').addClass("empty");
                $('#ucFreeSimLandingPages_divPostCode').removeClass("block errormsg clear");
                $('#ucFreeSimLandingPages_divPostCode').text('');
            }
            else {
                $('#ucFreeSimLandingPages_divPostCode').removeClass("empty");
                $('#ucFreeSimLandingPages_divPostCode').addClass("block errormsg clear");
                $('#ucFreeSimLandingPages_divPostCode').html('Wpisz kod pocztowy.');
            }
        });

        //Email ID
        $('#ucFreeSimLandingPages_TextBoxEmail').blur(function () {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var EmailID = $('#ucFreeSimLandingPages_TextBoxEmail').val();
            if (EmailID.length > 0) {
                if (reg.test(EmailID)) {
                    $('#ucFreeSimLandingPages_divEmail').addClass("empty");
                    $('#ucFreeSimLandingPages_divEmail').removeClass("block errormsg");
                    $('#ucFreeSimLandingPages_divEmail').text('');
                }
                else {
                    $('#ucFreeSimLandingPages_divEmail').removeClass("empty");
                    $('#ucFreeSimLandingPages_divEmail').addClass("block errormsg");
                    $('#ucFreeSimLandingPages_divEmail').html('Bitte gebe eine gültige E-Mail Adresse ein.');
                }
            }
            else {
                $('#ucFreeSimLandingPages_divEmail').removeClass("empty");
                $('#ucFreeSimLandingPages_divEmail').addClass("block errormsg");
                $('#ucFreeSimLandingPages_divEmail').html('Bitte gebe eine E-Mail Adresse ein.');
            }
        });

    });
</script>
<!-- ******************SEO Metrics End****************** -->
<a href="javascript: void(0)" id="ucFreeSimLandingPages_imgLogoLink" style="display: none" target="_blank" onclick="window.open(&#39;http://www.delightmobile.at&#39;, &#39;windowname1&#39;, &#39;width=900, height=700,scrollbars=yes&#39;); return false;">
    <div class="header-text-price-country">
        <span id="ucFreeSimLandingPages_lblPrice" class="text-price"></span>
        <span id="ucFreeSimLandingPages_lblCountry" class="text-country"> </span>
    </div>
</a>

<div class="new-form-box">
    <!--  <img src="/img-lp/pl/leftimg-poland.png" id="ucFreeSimLandingPages_imgRight" height="461" /> -->
    <div class="backnone1">
        <div class="tr_main">
            <div class="tr_label mob-adj1">
                <img src="/ppc/img-lp/delight_sim.png">
            </div>
            <div class="tr_input mob-adj2">
                <div class="box1">
                    <h2>
                        Zamów darmową kartę SIM 
(uniwersalnie zaprojektowana
karta standard/micro)
                    </h2>
                </div>
                <div class="errortext">
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                </div>
            </div>
        </div>
        <div class="tr_main">
            <div class="tr_label">
                <span id="ucFreeSimLandingPages_LabelFirstName" class="label">Imię:</span>
            </div>
            <div class="tr_input">
                <input name="ucFreeSimLandingPages$TextBoxFirstName" type="text" id="ucFreeSimLandingPages_TextBoxFirstName" class="landingInputLong"> <div id="ucFreeSimLandingPages_divFirstName">
            </div>
            </div>
           
            
        </div>
        <div class="tr_main">
            <div class="tr_label">
                <span id="ucFreeSimLandingPages_LabelLastName" class="label">Nazwisko:</span>
            </div>
            <div class="tr_input">
                <input name="ucFreeSimLandingPages$TextBoxLastName" type="text" id="ucFreeSimLandingPages_TextBoxLastName" class="landingInputLong">  <div id="ucFreeSimLandingPages_divLastName">
            </div>
            </div>
          
            
        </div>
        <div class="tr_main">
            <div class="tr_label">
                <span id="ucFreeSimLandingPages_LabelHouseNumber" class="label">Numer domu:</span>
            </div>
            <div class="tr_input">
                <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ucFreeSimLandingPages$ScriptManagerOne', 'form1', [], [], [], 90, '');
//]]>
</script>

                <input name="ucFreeSimLandingPages$TextBoxHouseNumber" type="text" id="ucFreeSimLandingPages_TextBoxHouseNumber" class="landingInputLong">
                <div id="ucFreeSimLandingPages_divHouseNumber">
                </div>
                
            </div>
        </div>
        <div class="tr_main">
            <div class="tr_label">
                <span id="ucFreeSimLandingPages_LabelStreet" class="label">Ulica:</span>
            </div>
            <div class="tr_input">
                <input name="ucFreeSimLandingPages$TextBoxStreet" type="text" id="ucFreeSimLandingPages_TextBoxStreet" class="landingInputLong">
                <div id="ucFreeSimLandingPages_divStreet">
                </div>
                
            </div>
        </div>
        <div class="tr_main">
            <div class="tr_label">
                <span id="ucFreeSimLandingPages_LabelCity" class="label">Miasto:</span>
            </div>
            <div class="tr_input">
                <input name="ucFreeSimLandingPages$TextBoxCity" type="text" id="ucFreeSimLandingPages_TextBoxCity" class="landingInputLong">     <div id="ucFreeSimLandingPages_divTown">
            </div>
            </div>
       
        </div>
        <div class="tr_main">
            <div class="tr_label">
                <span id="ucFreeSimLandingPages_LabelPostCode" class="label">Kod pocztowy:</span>
            </div>
            <div class="tr_input">
                <input name="ucFreeSimLandingPages$TextBoxPostCode" type="text" maxlength="4" id="ucFreeSimLandingPages_TextBoxPostCode" class="landingInputLong">
                <div id="ucFreeSimLandingPages_divPostCode">
                </div>
                
            </div>
        </div>
        <div class="tr_main">
            <div class="tr_label">
                <span id="ucFreeSimLandingPages_LabelEmail" class="label">Email:</span>
            </div>
            <div class="tr_input">
                <input name="ucFreeSimLandingPages$TextBoxEmail" type="text" id="ucFreeSimLandingPages_TextBoxEmail" class="landingInputLong">    <div id="ucFreeSimLandingPages_divEmail">
            </div>
            </div>
        
        </div>
        <div class="tr_main">
          
                
          
            <div class="control">
                <span id="ucFreeSimLandingPages_Label1" class="label">Klikając na przycisk poniżej potwierdasz, że zapoznałeś się z<a href="http://www.delightmobile.at/View/Info/TermsAndConditions.aspx" target="_blank"> Regulaminem </a> oraz <a href="http://www.delightmobile.at/View/Info/PrivacyPolice.aspx" target="_blank">Polityką Prywatności</a>.        </span>
            </div>
        </div>
        <div class="tr_main">
            <div class="tr_label">
                &nbsp;
            </div>
            <div class="tr_input">
                <a id="ucFreeSimLandingPages_linkButtonSubmit" class="btn-lp-proceed_br" href="javascript:__doPostBack(&#39;ucFreeSimLandingPages$linkButtonSubmit&#39;,&#39;&#39;)">Zamów kartę SIM</a>
            </div>
        </div>
        
        
    </div>
</div>
<div class="main-box">
    <div class="lp_leftimg">
        <script type="text/javascript">
            $(document).ready(function () {
                $('a').click(function () {
                    if ($(this).attr('id') == '') {
                        window.open(this.href, '', 'status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=1,scrollbars=1,width=948,height=640');
                        return false;
                    }
                });
            });
        </script>
        <img src="/ppc/img-lp/leftimg-poland.png" id="ucFreeSimLandingPages_imgLeft" class="left-image">
    </div>
</div>
<!-- 
        <div class="left-head">
            <img src="/img-lp/pl/page_header_sim.jpg" id="ucFreeSimLandingPages_imgHeader" width="378" height="78" />
        </div>

        -->

                

 
    </div>